from django.conf.urls import patterns, include, url

from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns(
    '',
    # Examples:
    # url(r'^$', 'varunbhat.views.home', name='home'),
    # url(r'^/', include('blog.urls')),

    url(r'^web/', include('website.urls')),
    url(r'^thinkpad/', include('thinkpad.urls')),
    url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
)
