"""
Django settings for varunbhat project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

PRODUCTION_ENV = os.getenv("PRODUCTION", 0)

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '@dpoay@8od0+u0+xo$%j6=r4y=+*&-zk9=!*69(24w10-mm54c'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []

AUTH_USER_MODEL = 'varunbhat.WebsiteUser'

# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # 'django.contrib.sites',
    'varunbhat',
    'blog',
    'website',
    'thinkpad',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'varunbhat.urls'

WSGI_APPLICATION = 'varunbhat.wsgi.application'

# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = dict()
if PRODUCTION_ENV == 1:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.mysql',
            'NAME': 'varunbhat',
            'USER': 'varunbhat',
            'PASSWORD': 'djptwm241',
            'HOST': 'localhost',
            'PORT': 3306
        },
        # 'users': {
        # 'NAME': 'user_data',
        # 'ENGINE': 'django.db.backends.mysql',
        # 'USER': 'mysql_user',
        # 'PASSWORD': 'superS3cret'
        # },
        # 'customers': {
        # 'NAME': 'customer_data',
        # 'ENGINE': 'django.db.backends.mysql',
        # 'USER': 'mysql_cust',
        # 'PASSWORD': 'veryPriv@ate'
        # }
    }
else:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.mysql',
            'NAME': 'varunbhat',
            'USER': 'varunbhat',
            'PASSWORD': 'akesix4@kl5',
            'HOST': 'varunbhatdb.cf4tqkayagyc.us-west-2.rds.amazonaws.com',
            'PORT': 3306
        },
    }

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'
