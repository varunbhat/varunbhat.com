from django.db import models
from varunbhat.models import WebsiteUser


class AcademicRecordsChoices:
    degrees = (
        (1, '10th Boards'),
        (2, 'Pre-University/12th boards'),
        (3, 'UnderGraduate'),
        (4, 'Graduate'),
        (5, 'Post Graduate')
    )
    gradingType = (
        (1, 'Percentage'),
        (2, 'GPA'),
        (3, 'Grades')
    )


class AcademicRecords(models.Model):
    profilee = models.ForeignKey(WebsiteUser)
    degree_id = models.IntegerField(choices=AcademicRecordsChoices.degrees, null=False)
    score_type = models.IntegerField(choices=AcademicRecordsChoices.gradingType, null=False)
    score = models.DecimalField(max_digits=5, decimal_places=2)
    grades = models.CharField(max_length=5)
    board = models.CharField(max_length=250, null=False)
    graduation_year = models.IntegerField()


class AchievementRecords(models.Model):
    profilee = models.ForeignKey(WebsiteUser)
    start_time = models.DateField(null=True)
    end_time = models.DateField(default=start_time)
    project_name = models.CharField(max_length=100)
    achievement = models.CharField(max_length=500)
    description = models.TextField()
    # links = models.()
    # pictures = models
